/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {colors: {
      primary: '#EF4444 ',
      dark: '#18181b',
      biru: {
        '300': '#417DF6',
        '400': '#315FB5',
        '450': '#033AAA',
        '500': '#003298',
      }
    },
    screen: {
        '2xl' : '1320px',
    },
},
  },
  plugins: [],
}
